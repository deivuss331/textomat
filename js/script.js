// chart
document.addEventListener('DOMContentLoaded', function () {
    var myChart = Highcharts.chart('chart-box', {

    title: {
        text: ''
    },

    subtitle: {
        text: ''
    },

    yAxis: {
        title: {
            text: ''
        }
    },

    xAxis: {
        accessibility: {
            rangeDescription: ''
        }
    },

    legend: {
        layout: 'vertical',
        align: 'right',
        verticalAlign: 'middle'
    },

    plotOptions: {
        series: {
            label: {
                connectorAllowed: false
            },
            pointStart: 1
        }
    },

    series: [{
        name: '',
        data: [300, 320, 380, 420, 500, 560, 600, 640, 660, 670, 650, 620, 580, 540, 500, 470, 440, 410, 400, 390,
        380, 390, 400, 440, 500, 600, 750, 850, 1000, 1120, 1220, 1280, 1300, 1310, 1330]
    }],

    responsive: {
        rules: [{
            condition: {
                maxWidth: 500
            },
            chartOptions: {
                legend: {
                    layout: 'horizontal',
                    align: 'center',
                    verticalAlign: 'bottom'
                }
            }
        }]
    }

    });
});

  // accordions
  var acc = document.getElementsByClassName("accordion");
  var i;

  for (i = 0; i < acc.length; i++) {
    acc[i].addEventListener("click", function() {
      this.classList.toggle("active");

      var more = this.nextElementSibling;
      if (more.style.display === "block") {
        more.style.display = "none";
      } else {
        more.style.display = "block";
      }
    });
  }

  // order tables
  var parent = document.getElementById("order-table-main");
  var button = document.getElementsByClassName("orders-table-switch");
  var tab = document.getElementsByClassName("orders-table-content");
  var c, d;

  tab[0].style.display = "block";
  parent.style.paddingBottom = tab[0].offsetHeight + "px";

  for (c = 0; c < button.length; c++) {
    button[c].addEventListener("click", function() {
      for (d = 0; d < tab.length; d++) {
        button[d].classList.remove("active");
        tab[d].style.display = "none";
      }

      this.classList.toggle("active");

      var next = this.nextElementSibling;
      if (next.style.display === "block") {
        next.style.display = "none";
      } else {
        next.style.display = "block";
        parent.style.paddingBottom = next.offsetHeight + "px";
      }
    });
  }